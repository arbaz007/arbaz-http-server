const http = require("http");
const path = require("path");
const fs = require("fs");
const url = require("url");
const { v4: uuid4 } = require("uuid");
const validStatusCode = require("./validStatusCode");
const httpStatusCodes = require("./validStatusCode");

const server = http.createServer((req, res) => {
  let parse = url.parse(req.url, true);
  let pathname = parse.pathname;
  let query = pathname.split("/").pop();
  console.log(pathname);
  if (pathname == "/html" && req.method == "GET") {
    fs.access(path.join(__dirname, "index.html"), fs.constants.F_OK, (err) => {
      if (err) {
        res.writeHead(404, { "Content-Type": "text/plain" });
        res.end(res.statusCode + " file not found");
        let error = new Error("file not found");
        throw error.message;
      } else {
        fs.readFile(path.join(__dirname, "index.html"), (err, data) => {
          res.writeHead(200, { "Content-Type": "text/html" });
          res.end(data);
        });
      }
    });
  } else if (pathname == "/uuid" && req.method == "GET") {
    const uuid = uuid4();
    res.writeHead(200, { "Content-Type": "text/html" });
    res.end(JSON.stringify({ uuid }));
  } else if (pathname == "/json" && req.method == "GET") {
    let user = {
      slideshow: {
        author: "Yours Truly",
        date: "date of publication",
        slides: [
          {
            title: "Wake up to WonderWidgets!",
            type: "all",
          },
          {
            items: [
              "Why <em>WonderWidgets</em> are great",
              "Who <em>buys</em> WonderWidgets",
            ],
            title: "Overview",
            type: "all",
          },
        ],
        title: "Sample Slide Show",
      },
    };
    if (!user) {
      res.writeHead(404, { "Content-Type": "text/plain" });
      res.end(404 + " user not found");
    } else {
      res.writeHead(200, { "Content-Type": "application/json" });
      res.end(JSON.stringify(user));
    }
  } else if (pathname.startsWith("/status") && req.method == "GET") {
    if (httpStatusCodes.includes(+query)) {
      res.writeHead(query, { "Content-Type": "application/json" });
      res.end(JSON.stringify({ status: query, msg: http.STATUS_CODES[query] }));
    } else {
      res.end("Status code is not valid");
    }
  } else if (pathname.startsWith("/delay") && req.method == "GET") {
    if (!isNaN(+query)) {
      setTimeout(() => {
        res.writeHead(200, { "Content-Type": "application/json" });
        res.end(
          JSON.stringify({
            status: 200,
            message: `Response sent after ${query} second`,
          })
        );
      }, query * 1000);
    } else {
      res.end(JSON.stringify({ error: "Not a Number error" }));
    }
  }
});

server.listen(5000, () => {
  console.log("listening");
});
